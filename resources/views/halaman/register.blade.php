@extends('layout.master')

@section('judul')
Sign Up Form    
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="firstName"><br><br>
    <label>Last Name:</label><br><br>
    <input type="text" name="lastName"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="other">Other<br><br>
    <label>Nationality:</label><br><br>
    <select name="nationality" id="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="British">British</option>
        <option value="American">American</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
    </form>
@endsection